import { pool } from 'pg';
import { graphql } from 'graphql';
import { withPostGraphileContext } from 'postgraphile';
import gql from 'graphql-tag';

export const CODE_QUERY = gql`
    {
        allCards(filter: {data: {contains: $code}}) {
            edges {
            node {
                data
            }
        }
    } 
`;

export async function performQuery(
  schema,
  query,
  variables,
  jwtToken,
  operationName
) {
  return await withPostGraphileContext(
    {
      pgPool: pool,
      jwtToken: jwtToken,
      jwtSecret: "...",
      pgDefaultRole: "..."
    },
    async context => {
      // Execute your GraphQL query in this function with the provided
      // `context` object, which should NOT be used outside of this
      // function.
      return await graphql(
        schema, // The schema from `createPostGraphileSchema`
        query,
        null,
        { ...context }, // You can add more to context if you like
        variables,
        operationName
      );
    }
  );
}