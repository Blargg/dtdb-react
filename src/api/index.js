const cors = require("cors");
const express = require("express");
const { postgraphile } = require("postgraphile");
const ConnectionFilterPlugin = require("postgraphile-plugin-connection-filter");

const connectionString = require("./config.js");

const app = express();

app.options('*', cors());

app.use(
  postgraphile(connectionString, "public", {
    appendPlugins: [ConnectionFilterPlugin],
    graphiql: true,
    enhanceGraphiql: true,
    dynamicJson: true,
    enableCors: true
  })
);

app.listen(5000);