import React, { Suspense, lazy } from 'react';
import { Route, Switch, Redirect } from 'react-router-dom'

import Container from 'react-bootstrap/Container'
import Row from 'react-bootstrap/Row'

import MainMenu from './components/MainMenu'
import Footer from './components/Footer'
import DeckSummary from './components/DeckSummary'

const About = lazy(() => import("./routes/About"));
const Card = lazy(() => import("./routes/Card"));
const Decklists = lazy(() => import("./routes/Decklists"));
const Home = lazy(() => import("./routes/Home"));
const MyDecks = lazy(() => import("./routes/MyDecks"));
const NoMatch = lazy(() => import("./routes/NoMatch"));
const Search = lazy(() => import("./routes/Search"));
const CardSet = lazy(() => import("./routes/Set"));

export const Routes = () => {
  return (
    <Container fluid="true" >
      <MainMenu />
      <Row className="d-flex justify-content-center">
        <Suspense fallback={<div>Loading...</div>}>
          <Switch>
            <Route exact path="/" render={() => <Home /> } /> 
            <Redirect from="/en/*" to="/*"></Redirect>
            <Route path="/card/:code" render={ (props) =>
                <Card code={props.match.params.code} /> 
              }/>
            <Route path="/set/:pack_code" render={(props) => <CardSet {...props} /> } />
            <Route path="/deck/:deck_code" render={props => <DeckSummary {...props}/> } />
            <Route exact path="/set" render={props => <CardSet {...props}/> } />
            {/* <Route exact path="/search" component={Search} />
            <Route exact path="/decklist" component={Decklists} />
            <Route exact path="/mydecks" component={MyDecks} />
            <Route exact path="/about" component={About} />
            <Route exact path="/about" component={Home} /> */}
            <Route render={() => <NoMatch /> } />
          </Switch>
        </Suspense>
      </Row>
      <Footer />
    </Container>
  )
}
