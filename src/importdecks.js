var pg = require('pg')
var mysql = require('mysql')

var PGUSER = 'dtdb'
var PGDATABASE = 'dtdb'
var config = {
  user: PGUSER, // name of the user account
  database: PGDATABASE, // name of the database
  max: 10, // max number of clients in the pool
  idleTimeoutMillis: 30000
}

var pool = new pg.Pool(config)

var con = mysql.createConnection({
  host: 'localhost',
  user: 'root',
  database: 'symfony'
})

var decksMsql = 'SELECT ' +
          'id,' +
          'user_id,' +
          'JSON_OBJECT(' +
              "'id', deck.id," +
              "'user', deck.user_id," +
              "'name', deck.name," +
              "'description', deck.description," +
              "'date_created', deck.datecreation," +
              "'date_updated', deck.dateupdate," +
              "'parent_deck_id', deck.parent_decklist_id" +
              ") as 'data'" +
              'FROM deck'

var deckslotMsql = `SELECT deckslot.deck_id, JSON_QUOTE(GROUP_CONCAT(JSON_OBJECT(card.code, JSON_OBJECT('quantity', deckslot.quantity, 'start', deckslot.start ) ) ) ) as data FROM deckslot INNER JOIN card on deckslot.card_id = card.id group by deck_id`

var decksPsql = 'INSERT into deck (id, user_id, data) VALUES ($1, $2, $3)'

var deckslotPsql = 'INSERT into deckslot (deck_id, data) VALUES ($1, $2)'

con.query(decksMsql, (err, result) => {
  if (err) throw err

  result.forEach((value) => {
    let values = [value.id, value.user_id, value.data]

    pool.query(decksPsql, values, (err, res) => {
      if (err) throw err
    })
  })
})

con.query(deckslotMsql, (err, result) => {
  if (err) throw err

  result.forEach((value) => {
    let values = [value.deck_id, value.data]

    pool.query(deckslotPsql, values, (err, res) => {
      if (err) throw err
    })
  })
})
