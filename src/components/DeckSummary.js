import React from 'react';

import Container from 'react-bootstrap/Container'

import { Query } from 'react-apollo'

import CardSummary from './CardSummary'

import gql from 'graphql-tag'

import ReactMarkdown from 'react-markdown';

import FactionIcon from './GameIcon';
import RankSuit from './RankSuit';


const deck_contents = {
  "01040": {"start": 1, "quantity": 1},
  "01045": {"start": 1, "quantity": 1},
  "01053": {"start": 0, "quantity": 1},
  "01073": {"start": 0, "quantity": 1},
  "01074": {"start": 0, "quantity": 1},
  "01075": {"start": 0, "quantity": 2},
  "01077": {"start": 0, "quantity": 1},
  "01100": {"start": 0, "quantity": 2},
  "01102": {"start": 0, "quantity": 4},
  "01103": {"start": 0, "quantity": 4},
  "01133": {"start": 0, "quantity": 1},
  "01134": {"start": 0, "quantity": 3},
  "01136": {"start": 0, "quantity": 3},
  "01144": {"start": 0, "quantity": 1},
  "01145": {"start": 0, "quantity": 1},
  "02008": {"start": 0, "quantity": 1},
  "02017": {"start": 0, "quantity": 2},
  "03008": {"start": 0, "quantity": 1},
  "03017": {"start": 0, "quantity": 2},
  "03018": {"start": 0, "quantity": 2},
  "03021": {"start": 0, "quantity": 3},
  "04011": {"start": 0, "quantity": 1},
  "05015": {"start": 1, "quantity": 1},
  "05019": {"start": 0, "quantity": 1},
  "07005": {"start": 1, "quantity": 1},
  "07007": {"start": 0, "quantity": 1},
  "07010": {"start": 0, "quantity": 1},
  "10030": {"start": 0, "quantity": 2},
  "10039": {"start": 0, "quantity": 1},
  "12005": {"start": 0, "quantity": 1},
  "13009": {"start": 0, "quantity": 1},
  "18006": {"start": 0, "quantity": 1},
  "18025": {"start": 0, "quantity": 2},
  "19002": {"start": 0, "quantity": 1},
  "19018": {"start": 1, "quantity": 1},
  "19026": {"start": 0, "quantity": 1},
  "19027": {"start": 0, "quantity": 1}
}

const deck = {
   "id":26829,
   "name":"The Sluckster Shakedown",
   "user":1057,"description":"",
   "date_created":"2016-10-05 16:58:28.000000",
   "date_updated":"2017-11-24 23:10:38.000000",
   "parent_deck_id":2226
};

const DeckSummary = (props) => {
    return(
      <Container>
        <h1 className="decklist-name">{deck.name}</h1>
        <hr />
          
        {deckContents()}

      </Container>
      );
}

const CARD_QUERY = gql`
query CardSummary($code: String!){
  allCards(filter: {data: {contains: {code: $code}}}) {
    edges {
      node {
        data
      }
    }
  }
}`

function deckContents(contents) {
  return(<div />)
}

export default DeckSummary;