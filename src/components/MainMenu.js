import React from 'react';

import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';

import Button from 'react-bootstrap/Button';
import Form from 'react-bootstrap/Form';
import FormControl from 'react-bootstrap/FormControl';

import Nav from 'react-bootstrap/Nav';
import NavDropdown from 'react-bootstrap/NavDropdown';
import Navbar from 'react-bootstrap/Navbar';

import gql from 'graphql-tag';
import { Query } from 'react-apollo';

import urls from '../data/urls';

const MainMenu = () => {
   return(
    <Row>
        <Col>
            <Navbar bg="black" variant="dark" className="d-flex justify-content-between">
                <Navbar.Brand href="/">
                    <img src="/icon.png" className="d-inline-block align-top p-1 dtdb-icon"
                        alt="DoomtownDB"/>DoomtownDB
                </Navbar.Brand>
                <Nav>
                    <Nav.Link href="/mydecks" className="px-3">My Decks</Nav.Link>
                    <Nav.Link href="/decklists" className="px-3">Decklists</Nav.Link>
                    <NavDropdown title="Cards" className="px-3">
                        <NavDropdown.Item href="/search" key="search">Search</NavDropdown.Item>
                        <NavDropdown.Divider key='search-divider'/>
                        {cardSets()}
                    </NavDropdown>
                    <NavDropdown title="Resources" className="px-3">
                        {externalUrls()}
                    </NavDropdown>
                </Nav>
                <Form inline className="search-box">
                    <FormControl type="text" placeholder="Search" className="mr-sm-2" />
                    <Button variant="outline-light">Search</Button>
                </Form>
                <Nav> 
                    <Nav.Link href="/account" className="px-5">Account</Nav.Link>
                </Nav>
            </Navbar>
            <hr />
        </Col>
    </Row>
    );
};  

function cardSets() {
    const SET_QUERY = gql`
       {
        allSets {
          edges {
            node {
              data
            }
          }
        }
      }`;

      return (
        <Query query={SET_QUERY} errorPolicy="all">
            {({ loading, error, data }) => {
            if (loading) return "Loading...";
            if (error) return `Error! ${error.message}`;
    
            let sets = [];
            let rows = [];

            let promodivider = true;

            let counter = 0;
             
            data.allSets.edges.forEach(edge => {
                sets.push(edge.node.data);
            });
            
            sets = sets.sort((a,b) => { if (a.code === 'alt') return -1; return 1})

            sets.forEach(set => {
                rows.push(<NavDropdown.Item href={set.url} key={set.code}>{ counter > 0 ? counter + " - " : false }{ set.name}</NavDropdown.Item>);
            
                counter++;

                if(promodivider) {
                    rows.push(<NavDropdown.Divider key='promo-divider'/>);
                    promodivider = false;
                }
            });          
            
            return rows;
            }}
        </Query>
        );
}

function externalUrls() {

    var rows = [];

    urls.forEach( function(json) {

        if(json.title === 'divider') {
            rows.push(<NavDropdown.Divider key={json.key}/>)
        }
        else {
            rows.push(<NavDropdown.Item href={json.url} key={json.key}>{json.title}</NavDropdown.Item>)
        }
            
    });

    return rows;
}

export default MainMenu;