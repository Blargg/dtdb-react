import React from 'react';

import Card from 'react-bootstrap/Card';

import ReactMarkdown from 'react-markdown';

import FactionIcon from '../GameIcon';
import RankSuit from '../RankSuit';

const CardSummary = (props) => {

    let card = props.card;

    if(card === undefined) {
        return ""
    }

    return(
        <Card style={{ width: '45rem' }}>
            <Card.Header className="m-0">
                <RankSuit card={card} />
                <Card.Title className="m-0"><a href={card.url} className="text-reset">{card.title}</a></Card.Title>
            </Card.Header>
            <Card.Body className="d-flex align-items-center">
                <div style={{ width: '40rem' }} className="p-3">
                    <Card.Subtitle className="mb-2 card-type p-1"></Card.Subtitle>
                    <Card.Subtitle className="mb-2 text-muted p-2">{statLine(card)}</Card.Subtitle>
                    <Card.Subtitle className="mb-2 card-keywords p-1">{card.keywords}</Card.Subtitle> 
                    {card.text ? <Card.Text as="div" className="mb-2 card-text p-3"><ReactMarkdown source={card.text} /></Card.Text> : false }
                    {card.flavor ? <Card.Subtitle className="mb-2 card-flavor p-3">{card.flavor}</Card.Subtitle> : false } 
                </div>
                <Card.Img src={card.imagesrc} className="p-3 card-summary-image"></Card.Img>
            </Card.Body>
            <Card.Footer className="d-flex justify-content-around align-items-center">
                <FactionIcon />
                <Card.Link className="text-muted card-set-name p-1">{card.pack} #{card.number}</Card.Link>
                <Card.Link className="text-muted card-illustrator p-1">Illus. {card.illustrator}</Card.Link>
            </Card.Footer>
        </Card>
    );
}

function statLine(card) {

    let stats = [];
    
    stats.push(card.type);

    if(card.shooter) {
        stats.push(card.shooter + " " + card.bullets);
    }
    
    if(card.control) {
        stats.push("Control " + card.control);
    }

    if(card.influence) {
        stats.push("Influence " + card.influence);
    }

    if(card.production) {
        stats.push("Production " + card.production);
    }

    if(card.cost) {
        stats.push("Cost " + card.cost);
    }

    if(card.upkeep) {
        stats.push("Upkeep " + card.upkeep);
    }

    return(stats.join(" • "));
}

export { CardSummary };