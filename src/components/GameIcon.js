import React from 'react';

const GameIcon = (props) => {
    
    let icon;
    
    switch(props.icon) {
        case 'anarchists':
            icon = '/anarchists-20.png';
            break;
        case '1stpeoples':
            icon = "/1stpeoples-20.png";
            break;
        case 'fearmongers':
            icon = "/fearmongers-20.png";
            break;
        case 'lawdogs':
            icon = "/lawdogs-20.png";
            break;
        case 'outlaws':
            icon = "/outlaws-20.png";
            break;
        case 'entrepreneurs':
            icon = "/entrepreneurs-20.png";
            break;
        default:
            icon = "/neutral-20.png";
    }

    return(<img src={icon} alt={icon} className={icon + "icon"} />);
}

export default GameIcon;