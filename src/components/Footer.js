import React from 'react';

import Jumbotron from 'react-bootstrap/Jumbotron';
import Container from 'react-bootstrap/Container';

const Footer = () => {
   return(
    <Jumbotron fluid className="mb-0 mt-5 p-3 footer">
        <Container>
            <p>Designed, built, and maintained by <a href="mailto:admin@dtdb.co">Blargg</a>.</p>
            <p>Original site design by <a href="//twitter.com/alsciende">@alsciende</a> and maintained by <a href="//twitter.com/platypusdt">@PlatypusDT</a>.</p>
            <p>Bug reports and Feature Requests on <a href="https://bitbucket.org/Blargg/dtdb/issues?status=new&status=open" target="_blank" rel="noopener noreferrer">BitBucket</a></p>
            <p>DoomtownDB is <a href="/contributors">supported by the community</a> to stay ad-free. <a href="https://paypal.me/Blargg">Donate here.</a></p>
            <p>Doomtown: Reloaded and Deadlands copyright <a href="//peginc.com"><img src="/pinnacle.png" width="143" height="40" alt="Pinnacle Entertainment Group" /></a>.</p>
        </Container> 
    </Jumbotron>
    );
} 


export default Footer;