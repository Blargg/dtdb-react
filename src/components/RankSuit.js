import React from 'react';
import PropTypes from 'prop-types';

const RankSuit = (props) => {

    let card = props.card;

    if(card.suit === null) {
        return null;
    }
    
    let rank = card.rank;
    
    switch(card.rank) {
        case 1:
            rank = 'A';
            break;
        case 11:
            rank = 'J';
            break;
        case 12:
            rank = 'Q';
            break;
        case 13:
            rank = 'K'
            break;
        default:
            break;    
    }

    return(<span className="float-right">
                <span className={"card-face card-face-" + card.suit.toLowerCase()}>
                    <span className="card-rank font-weight-bold">{rank}</span>
                    <span className={"card-suit suit-" + card.suit.toLowerCase()}></span>
                </span>
            </span>);
}

RankSuit.propTypes = {
  card: PropTypes.object
};

export default RankSuit;



