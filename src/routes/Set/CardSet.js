import React from 'react';
import gql from 'graphql-tag';
import { Query } from 'react-apollo';

import Col from 'react-bootstrap/Col';
import Table from 'react-bootstrap/Table';

import RankSuit from '../../components/RankSuit';
import GameIcon from '../../components/GameIcon';


const CardSet = props => {
    return (
      <Col md="10" className="set-list">
        <Table striped bordered hover>
          <thead>
            <tr>
              <th>Title</th>
              <th>Faction</th>
              <th>Type</th>
              <th>Value</th>
              <th>Cost</th>
              <th>Keywords</th>
              <th>Set</th>
            </tr>
          </thead>
          <tbody>
            { cardSetSummary({pack_code: props.match.params.pack_code}) } 
          </tbody>
        </Table>
      </Col>
    );
};

const CARD_SET_QUERY = gql`
query CardSetSummary($pack_code: String!) {
  allCards(filter: {data: {contains: {pack_code: $pack_code}}}) {
    edges {
      node {
        data
      }
    }
  }
}`;

function cardSetSummary(code) {

    return (<Query query={CARD_SET_QUERY} variables={code} errorPolicy="all">
        {({ loading, error, data }) => {
        if (loading) return(<tr><td>Loading</td></tr>);
        if (error) return `Error! ${error.message}`;
      
        let cards = [];
        let rows = [];  

        data.allCards.edges.forEach(edge => {
          cards.push(edge.node.data);
        });
      
        cards = cards.sort((a,b) => { return a.number - b.number });

        cards.forEach(card => {

          rows.push(
            <tr key={card.code}>
              <td><a href={card.url} className='text-reset set-card-title'>{card.title}</a></td>
              <td><GameIcon icon={card.gang_code} /></td>
              <td>{card.type}</td>
              <td><RankSuit card={card} /></td>
              <td>{card.cost}</td>
              <td>{card.keywords}</td>
              <td>{card.pack} {card.number}</td>
            </tr>)
        });


        return rows;

        }}
    </Query>
    );
}

export { CardSet };