import React from 'react'
import PropTypes from 'prop-types'

import Col from 'react-bootstrap/Col'

import { Query } from 'react-apollo'

import CardSummary from '../../components/CardSummary'

import gql from 'graphql-tag'

const GameCard = props => {

  if(props.code === undefined)
  {
    return <div />
  }

  return (
    <Col>
      {cardSummary(props.code)}
    </Col>
  )
};

GameCard.propTypes = {
  code: PropTypes.string.isRequired
};

const CARD_QUERY = gql`
query CardSummary($code: String!){
  allCards(filter: {data: {contains: {code: $code}}}) {
    edges {
      node {
        data
      }
    }
  }
}`

function cardSummary (code) {
  return (
    <Query query={CARD_QUERY} variables= { { code } } errorPolicy="all">
      {({ loading, error, data }) => {
        if (loading) return 'Loading...'
        if (error) return `Error! ${error.message}`

        let cards = []
        data.allCards.edges.forEach(edge => {
          cards.push(<CardSummary card={edge.node.data} key={edge.node.data.code}></CardSummary>)
        })

        return (
          <div>
            { cards }
          </div>
        )
      }}
    </Query>
  )
}

export { GameCard };
